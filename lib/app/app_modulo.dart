import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:slidy_modular/app/app_controller.dart';
import 'package:slidy_modular/app/app_widget.dart';
import 'package:slidy_modular/app/modulos/inicial/inicial_modulo.dart';

import 'compartilhados/utils/constantes.dart';
//modulo principal pai de todos os modulos
class AppModulo extends MainModule {
  //chamar os controller, regra de negocios, para injeção
  @override
  List<Bind> get binds => [
        Bind((i) => AppController()),
        Bind((i) => Dio(BaseOptions(baseUrl: URL_BASE))),
      ];

  //rodas nomeadas com o nome da roda e o widget ou classe que ela chama
  @override
  List<Router> get routers => [
        Router("/", module: InicialModulo()),
      ];

  //widget principal pai de todos
  @override
  Widget get bootstrap => AppWidget();
}
