import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

//widget principal pai de todos
class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Slidy Modular",
      initialRoute: '/',
      onGenerateRoute: Modular.generateRoute,
    );
  }
}