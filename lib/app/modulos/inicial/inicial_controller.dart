import 'package:mobx/mobx.dart';

import 'models/poke_model.dart';
import 'repositorios/poke_repositorio.dart';
part 'inicial_controller.g.dart';

class InicialController = _InicialControllerBase with _$InicialController;

abstract class _InicialControllerBase with Store {

  final PokeRepositorio repositorio;

  @observable
  ObservableFuture<List<PokeModel>> pokemons;
  

  _InicialControllerBase(this.repositorio){
    pokemons = repositorio.buscarPokemons().asObservable();
  }
    
}