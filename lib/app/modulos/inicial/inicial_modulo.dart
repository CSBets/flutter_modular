import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:slidy_modular/app/modulos/inicial/inicial_page.dart';

import 'inicial_controller.dart';
import 'repositorios/poke_repositorio.dart';

class InicialModulo extends ChildModule {
  @override
  List<Bind> get binds => [
    Bind((i) => InicialController(i.get<PokeRepositorio>())),
    Bind((i) => PokeRepositorio(i.get<Dio>())),
  ];

  @override
  List<Router> get routers => [
    Router('/', child: (_, args)=> InicialPage()),
  ];
}
