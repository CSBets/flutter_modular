import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'inicial_controller.dart';

class InicialPage extends StatefulWidget {
  @override
  _InicialPageState createState() => _InicialPageState();
}

class _InicialPageState extends State<InicialPage> {
  
  var inicialController = Modular.get<InicialController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey[600],
      appBar: AppBar(
        backgroundColor: Colors.blueGrey[900],
      ),
      body: Observer(builder: (_) {
        if (inicialController.pokemons.error != null) {
          return Center(
            child: Text("Algo esta errado"),
          );
        } else if (inicialController.pokemons.value == null) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else {
          var lista = inicialController.pokemons.value;
          return ListView.builder(
            itemCount: lista.length,
            itemBuilder: (_, index) => ListTile(title: Text(lista[index].nome),),
          );
        }
      }),
    );
  }
}
