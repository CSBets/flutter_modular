import 'package:dio/dio.dart';
import 'package:slidy_modular/app/modulos/inicial/models/poke_model.dart';

class PokeRepositorio {
  final Dio dio;

  PokeRepositorio(this.dio);

  Future<List<PokeModel>> buscarPokemons() async {
    var resposta = await dio.get('/pokemon');
    List<PokeModel> lista  = [];
    for (var json in (resposta.data['results'] as List)) {
      PokeModel model = PokeModel(json["name"]);
      lista.add(model);
    }
    return lista;
  }
}
