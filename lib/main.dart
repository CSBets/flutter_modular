import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'app/app_modulo.dart';

void main() => runApp(ModularApp(module: AppModulo(),));